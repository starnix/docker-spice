## About

This is a wrapper around the starnix/docker-spice container and is
meant to be used on OSx systems in order to provide a *hopefully*
reliable way of using console.vv (Spice) files from RHEV/Ovirt.

It requires that XQuartz is installed on the system as well as docker
and is simply a CentOS container with virt-viewer (remote-viewer)
installed.

The 'Process Flow' is:

1. Start XQuarts if not already started
2. Create a hopefully valid display variable for the container
3. Allow host connections
4. Start contianer setting DISPLAY variable, mounting console.vv file, and execute remote-viewer passing the console.vv file as an argument.
4. Exit and remove container when remote-viewer stops.


** This does not stop/kill Xquartz on exit.


## Installation and first use

The bin/dspice script can be given the 'install' option which creates
a symlink to itself in ~/bin, /usr/local/bin, or /bin depending on
what is found first.

To Install simply run:
```bash
bin/dspice install
```

To Run:

Execute: dspice connect 'Full path to console.vv file'

Example:

```bash
dspice connect /Users/daibhidh/Downloads/console.vv
```



